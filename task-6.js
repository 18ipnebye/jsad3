"use strict";

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
};

const detailedEmployee = {
    ...employee,
    age: 32,
    salary: 1500
}

console.log(detailedEmployee);